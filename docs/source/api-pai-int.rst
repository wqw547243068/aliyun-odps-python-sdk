.. _apipaiint:


PAI References
==============

.. autoclass:: odps.pai.PAIContext
    :members:

.. autoclass:: odps.pai.DataSet
    :members:

.. autoclass:: odps.pai.TrainedModel
    :members:

.. autoclass:: odps.pai.algorithms.base_algo.BaseSupervisedAlgorithm
    :members:

Classification
--------------

.. automodule:: odps.pai.classifiers
    :members:

Regression
----------

.. automodule:: odps.pai.regression
    :members:

Clustering
----------

.. automodule:: odps.pai.clustering
    :members:

Preprocessing
-------------

.. automodule:: odps.pai.preprocess
    :members:

NLP
----

.. automodule:: odps.pai.nlp
    :members:

Complex Networks
----------------

.. automodule:: odps.pai.network
    :members:

Metrics
-------

.. automodule:: odps.pai.metrics.classification
    :members:

.. automodule:: odps.pai.metrics.regression
    :members:

Pipeline
--------

.. automodule:: odps.pai.pipeline
    :members:


Customization
-------------

.. automodule:: odps.pai.algorithms
    :members:
